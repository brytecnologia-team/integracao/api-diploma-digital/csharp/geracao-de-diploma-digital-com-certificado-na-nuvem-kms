# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-06-03

### Added

- A C# DIGITAL DIPLOMA with KMS certificate generation example. 

[1.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-diploma-digital/c-sharp/geracao-de-diploma-digital-com-certificado-na-nuvem-kms/-/tags/1.0.0

## [2.0.0] - 2022-05-10

### Added

- A C# DIGITAL DIPLOMA with KMS certificate generation example. Now with separate solutions for Public Diploma XML, Academic Documentation and Academic Records XML

[2.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-diploma-digital/c-sharp/geracao-de-diploma-digital-com-certificado-na-nuvem-kms/-/tags/2.0.0
