﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="WebApplication1.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Exemplo Framework Rest(c# com suporte ao .net core)</title>
    <link rel="stylesheet" href="Assets/css/bootstrap.min.css">

    <style media="screen">
        .extension-message {
            padding-top: 55px;
            padding-bottom: 55px;
        }
        .btn-extension-install {
            margin-bottom: 55px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server"> 
        
        <div class="container">
            <header class="header clearfix">
                <h2 class="text-muted"><img src="Assets/imgs/marca-signer-150dpi.png" style="width: 40px;" /> Exemplo Diploma Digital (XML) com Certificado na Nuvem</h2>
            </header>
                <div class="row">
                    <h3>Passo Único:</h3>
                    <p>Escolha qual tipo de assinatura de XML Histórico Escolar Digital será feita e clique no botão baixo para gerar a assinatura.</p>
                    <p>Atenção: Para Histórico Escolar Digital Parcial, deve ser gerada apenas a assinatura PF da Secretaria da IES Emissora. Para Histórico Integral, deverá ser executado ambos os fluxos PF e PJ, nessa sequencia.</p>
                    <asp:DropDownList ID="DropDownRole" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="PF: Representante da Secretaria" Value="HISTSECRETARIA" />
                    <asp:ListItem Text="PJ: IES Emissora" Value="HISTIESEMISSORA" />
                    </asp:DropDownList>
                    <p>Clique no botão baixo para gerar a assinatura.</p>
                </div>
                <div class="row">
                    <div class="col-md-offset-4 col-md-4" style="text-align: center;">
                        <asp:Button class="btn btn-lg btn-primary" ID="Button1" runat="server" OnClick="AssinarKMS_Click" Text="Assinatura com Certificado na Nuvem" />
                    </div>
                </div>

                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>

                        <div style="padding-bottom: 5px"></div>
                        <div class="row">
                            Saída Serviço (Diploma Digital via KMS)<br/>
                            <asp:TextBox ID="TextBoxSaidaInicializar" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                        </div>
                      
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button1" />
                    </Triggers>
                </asp:UpdatePanel>
                
                    
    </form>
    <footer>
        <div class="container">
            <hr>
            <p>&copy; 2021 BRy Tecnologia S.A</p>
        </div>
    </footer>
    <script src="Assets/js/jquery-3.2.1.min.js"></script>
    <script src="Assets/js/bootstrap.min.js"></script>
    <script src="Assets/js/script-customizavel.js"></script>
</body>
</html>
