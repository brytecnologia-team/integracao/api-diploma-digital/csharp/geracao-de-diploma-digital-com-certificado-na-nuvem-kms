using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebApplication1
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        //Você pode emitir um token valido em: https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insira seu token JWT em INSERT_VALID_ACCESS_TOKEN

        static string INSERT_VALID_ACCESS_TOKEN = "";
        static string kmsType = "BRYKMS";  //Valores Disponíveis: BRYKMS, GOVBR, DINAMO
        static string kmsPin = "SenhaDoCompartimentoDeCertificadosEmBase64Aqui";
        //static string kmsToken = "TokenPréAutorização/ATokenDinamo"
        static string kmsUuid = "UUIDDoCertificadoDesejadoAqui";
        //static string kmsUser = "12345678910"; 

        //Variáveis somente para caso seja certificado hospedado no HSM Dinamo
        //static string kmsPkey = "UUIDDaChavePrivada";
        //static string otp = "OTPDinamo";

        static string NAMESPACE = "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd";
        static string DADOSDIPLOMA = "DadosDiploma";
        static string DADOSREGISTRO = "DadosRegistro";
        static string INCLUDEXPATH = "includeXPathEnveloped";
        //static string DADOSDIPLOMANSF = "DadosDiplomaNSF"; //Caso seja Instituicao Federal
        //static string DADOSREGISTRONSF = "DadosRegistroNSF"; //Caso seja Instituicao Federal

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AssinarKMS_Click(object sender, EventArgs e)
        {
            try
            {
                string tipoAssinatura = this.DropDownRole.SelectedValue;
                this.DropDownRole.Enabled = false;

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                //Chamando a função para iniciar
                string responseInitialize = AssinarKMS(tipoAssinatura).Result;

                Resposta responseInitializeObj = new Resposta();
                responseInitializeObj = Deserialize<Resposta>(responseInitialize);

                //Se ocorrer algum erro na desserialização, saberemos aqui
                if (responseInitializeObj.documentos == null)
                {
                    this.TextBoxSaidaInicializar.Text = responseInitialize;
                    throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");
                }
                else
                {
                    //Popular o Textbox no front-end com os dados da resposta
                    this.TextBoxSaidaInicializar.Text = responseInitializeObj.documentos[0].links[0].href;  
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }


        public async static Task<string> AssinarKMS(string tipoAssinatura)
        {
            //Aqui montamos a requisição com os dados no formado Multipart
            var requestContent = new MultipartFormDataContent();

            //Define as mudanças na request para cada etapa da assinatura do Diploma
            string tagNodo = "nenhum";

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string nomeArquivo = null;
            switch (tipoAssinatura)
            {
                case "PESSOAFISICA":
                    tagNodo = "registro";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
                case "REGISTRADORADIPL": //Caso IES Registradora XML Diplomado
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
            }

            //Demais Atributos
            string hashAlgorithm = "SHA256";
            string returnType = "LINK";
            string operationType = "SIGNATURE";
            string binaryContent = "false";
            string signatureFormat = "ENVELOPED";
            string nonce = "123456";

            requestContent.Add(new StringContent(nonce), "nonce");
            requestContent.Add(new StringContent(hashAlgorithm), "hashAlgorithm");
            requestContent.Add(new StringContent(returnType), "returnType");
            requestContent.Add(new StringContent(operationType), "operationType");
            requestContent.Add(new StringContent(binaryContent), "binaryContent");
            requestContent.Add(new StringContent(operationType), "operationType");
            requestContent.Add(new StringContent(signatureFormat), "signatureFormat");

            //Pode se adicionar mais de um arquivo aqui
            var fileStream = new FileStream(path + "./" + nomeArquivo, FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);
            requestContent.Add(streamContentDocument, "originalDocuments[0][content]", nomeArquivo);
            requestContent.Add(new StringContent("1"), "originalDocuments[0][nonce]");

            if (tagNodo == "diploma")
            {
                requestContent.Add(new StringContent(DADOSDIPLOMA), "originalDocuments[0][specificNode][name]");
            }
            else if (tagNodo == "registro")
            {
                requestContent.Add(new StringContent(DADOSREGISTRO), "originalDocuments[0][specificNode][name]");
            }

            //Não enviará o Namespace no caso onde nodo assinado é o principal (raiz)
            if (tagNodo != "nenhum")
            {
                requestContent.Add(new StringContent(NAMESPACE), "originalDocuments[0][specificNode][namespace]");
            }


            HttpClient client = new HttpClient();
           
            //client.DefaultRequestHeaders.Add("fw_credencial", credencial);
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);
            client.DefaultRequestHeaders.Add("kms_type", kmsType);

            //Dados para acesso ao certificado no KMS
            KmsData kmsData = new KmsData();

            //Caso default é uso do PIN do compartimento do certificado no BRy KMS / Senha do Login de usuário DINAMO
            kmsData.pin = kmsPin;
            //Caso seja um token de pré autorização ou AToken dinamo, descomentar abaixo e comentar a linha acima com o kmsPin
            //kmsData.token = kmsToken;
            //Caso seja um OTP dinamo, descomentar abaixo e comentar as linhas acima
            //kmsData.otp = otp;

            //UUID do certificado no BRy KMS / HSM Dinamo
            kmsData.uuid_cert = kmsUuid;

            //Caso a autenticação seja o CPF do BRy KMS ou o Login do HSM dinamo, descomentar abaixo
            //kmsData.user = kmsUser;

            //Caso seja HSM Dinamo, descomentar abaixo
            //kmsData.uuid_pkey = kmsPkey;

            string kms_data = Serialize(kmsData);

            requestContent.Add(new StringContent(kms_data), "kms_data");

            //Envia os dados de inicialização da assinatura em formato Multipart
            var response = await client.PostAsync("https://hub2.bry.com.br/api/xml-signature-service/v1/signatures/kms", requestContent).ConfigureAwait(false);
            
            return await response.Content.ReadAsStringAsync().ConfigureAwait(false); 
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }

    [DataContract]
    public class Resposta
    {
        [DataMember]
        public string identificador;

        [DataMember]
        public string quantidadeAssinaturas;

        [DataMember]
        public List<Documentos> documentos;

        public Resposta()
        {
            this.documentos = new List<Documentos>();
        }
    }

    [DataContract]
    public class Documentos
    {
        [DataMember]
        public string hash;

        [DataMember]
        public List<Link> links;

        public Documentos()
        {
            this.links = new List<Link>();
        }
    }

    [DataContract]
    public class Link
    {
        [DataMember]
        public string rel;

        [DataMember]
        public string href;
    }

    [DataContract]
    public class KmsData
    {
        [DataMember]
        public string pin;

        [DataMember]
        public string uuid_cert;

        [DataMember]
        public string uuid_pkey;

        [DataMember]
        public string user;

        [DataMember]
        public string token;

        [DataMember]
        public string otp;
    }
}
